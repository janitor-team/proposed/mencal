Kyberdigi Labs                                                                  Menstruation calendar

                                                                   by C. McCohy <mccohy@kyberdigi.cz>
                                                                                  Actual version: 2.3

Use your computer for superb timing of all your activities                                     cz en

[folder_ope] Kyberdigi Labs   Menstruation calendar
*[folder_ope] Projects
**[folder_ope] Mencal         Mencal is a simple variation of the well-known unix command cal. The
***[folder] Requirements      main difference is that you can have some periodically repeating days
***[folder] License           highlighted in color. This can be used to track menstruation (or other)
***[folder] Download          cycles conveniently.
***[folder] Installation      ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
***[folder] Running mencal    
***[folder] Examples          Requirements
***[folder] Known bugs
***[folder] Changelog         This program is written in the perl language. It also uses the unix
***[folder] Archive           locales and the system commands strftime and mktime. I don't know which
***[folder] Misc              systems have all those, but at least any linux distribution should
                              provide you with everything necessary for running mencal.
[folder_ope] Visitors         ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
*[folder]Counter              
                              License

                              The software may be freely distributed under the terms of the GPL.
                              You can read the license here.

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Download

                              You can download the complete tarball via HTTP right here.

                              mencal-2.3.tar.gz

                              size: 19169 bytes
                              MD5: e0fa5665b8f7772521bb95379bfdec20

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Installation

                              First: unpack the archive

                              $ tar xzf mencal-2.3.tar.gz

                              Now enter the directory mencal-2.3 that was created.

                              $ cd mencal-2.3

                              And that's about all. This directory also contains a tiny README file,
                              a copy of the license and the most important file mencal. You can run
                              this program even as an unprivileged user with the command ./mencal.
                              You can copy the file to wherever it's convenient for you and run it
                              from there.

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Running mencal

                              You run the program like this:

                              $ mencal [options] [file1 file2 ... -c CONF1 -c CONF2 ... ]

                              Display options:

                                • -m or --monday - take monday as the first day of the week (sunday
                                  is the default)
                                • -1 - print the current month (default)
                                • -3 - print the previous, current and next month
                                • -y [yyyy] - print a calendar for the whole year yyyy. If the year
                                  is not given, the current year is used.
                                • -q or --quiet - don't print the introduction
                                • -n or --nocolor - don't use color in the output
                                • -i COLOR or --icolor COLOR - intersection color (the default is
                                  red, other possible are green, blue, yellow, violet, cyan, shiny
                                  (intense white) and bold (not really a color))

                              Menstruation options:
                              -c or --config s=[YYYY]MMDD,l=DD,d=T,n=NAME,f=FILE,c=COLOR

                              The second argument is a comma separated list of options. No spaces are
                              allowed in this list. If no name is specified, 'Unknown' is used.
                              Various -c options or filenames can be set.

                                • s,start=[YYYY]MMDD - začátek cyklu (standardně aktuální den)
                                • l,length=DD - length of period in days (default 28)
                                • d,duration=T - duration of menstruation in days (default 4)
                                • n,name=NAME - name of subject
                                • f,file=FILE - filename to save configuration to (only menstruation
                                  related variables will be saved)
                                • c,color=COLOR - color used for menstruation days of subject
                                  (available colors are the same as with --icolor, the switch -n will
                                  cause this color setting to be ignored)

                              Informational options:

                                • -h or --help - prints help on options
                                • -V or --version - prints version information

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Examples

                                • Example 1 - a simple example of three current months
                                • Example 2 - an example of a full setting of a particular
                                  mentruation cycle in an unusual yellow color
                                • Example 3 - a calendar for a whole year for two persons, the
                                  settings for one are taken from the file maruskarc, the days, when
                                  both persons are menstruating, are marked red
                                • Example 4 - the same settings as example 3, no colors (suitable for
                                  printing or for some monochrome terminals), without the starting
                                  info
                                • Example 5 a Example 6 - a bit over-done ...

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Known bugs

                              Incorrect day names displaying
                              In some cases of special locales setings the day names are displayed
                              incorrectly. For example if one uses hu_HU, some day names have only
                              one character, which can cause table damage. Thank to Peter Gervai for
                              bug info.
                              Solved: upgrade to version 2.3.

                              Error message typo
                              Micah Anderson wrote about a typo in one of error messages you can see
                              after bad parameter settings.
                              Solved: upgrade to version 2.2.

                              The 'more' command
                              The only real problem occurs when you are using the command more on the
                              colored output, e.g. $ mencal -y -c c=red | more ... it all gets messed
                              up
                              Solved: use more -f or less -R. Thanks to Peter K. Gale for this hint.

                              The year must be greater than 1900
                              Regrettably you can't use years below 1900. The cause is that perl
                              works with years decremented by 1900. So if you use the year 2002, perl
                              has to convert it to 102. And it of course refuses to use negative
                              numbers. If you have any ideas how to work around this, please tell me.

                              Localisation
                              The names of days and months are taken from locales, so it would
                              probably be nice to localize the help and program messages. There are
                              not that many messages, in fact. Well, next time. ;-)

                              Color printing
                              I have some messages, that it is impossible to print the colored output
                              on a printer ($ mencal | lpr), that the output on the paper looks
                              really bad. It's possible. I'm sorry, but I don't have any printer to
                              look at it and take care about. Use the -n switch for non-colored
                              output.

                              $ mencal -n | lpr

                              Year change
                              While using the -3 switch for printing of three actual months (i.e.
                              november, december, january) some month is not printed.
                              Solved: upgrade to version 2.1, where this bug is already fixed. Thanks
                              to Jára Altmann for bugreport.

                              Bugs from version 1.1
                              There's only a few lines left from 1.0 and the rest was completely
                              rewritten. The old bugs seem to be gone. The configuration doesn't
                              become invalid after one year and you don't have to set it up again.
                              The brain-damage of the old configuration files has also been
                              eliminated.

                              If you think you found a bug, don't hesitate to use your favourite mail
                              client and mail me.

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Changelog


                              2.3 (Jul 17th, 2003)

                              - fixed incorrect displaying of some day names

                              2.2 (May 22nd, 2003)

                              - fixed error message typo

                              2.1 (December 9th, 2002)

                              - bugfix: some months are not printed if '-3' is used
                              - optimization of counting menstruation days

                              2.0 (March 29th, 2002)

                              - initial primitive version total rewrited
                              - more configurations available
                              - merging of configurations
                              - other program switches
                              - non-colored output
                              - actual three months can be printed

                              1.1 (October 25th, 2000)

                              - configuration can be saved
                              - some minor bugfixes
                              - localization (use of locales)

                              1.0 (mesozoic era)

                              - initial primitive version

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Archive

                                • mencal-2.3.tar.gz
                                  Size: 19169, MD5: e0fa5665b8f7772521bb95379bfdec20
                                • mencal-2.2.tar.gz
                                  Size: 19163, MD5: c95eead531482b82f219d9e48c4e8cf7
                                • mencal-2.1.tar.gz
                                  Size: 19157, MD5: 6fc94b9174eb2ac71084b8848d4f5a3d
                                • mencal-2.0.tar.gz
                                  Size: 19109, MD5: abd648a8b195648b2c9bf0fa889ee305
                                • mencal-1.1.tar.gz
                                  Size: 18250, MD5: e862c88897e5d817762ff5bb5054c4a4

                              ━━━━━━━━━━━━━━━━━━━━━━━━━━━━
                              
                              Misc

                              I thank Vinil for the his intensive cooperation, inspiration and
                              propagation (the quote about timing is from him too) and JD for
                              testing, moral backing and correction.

                              And here's an inspiring response from the maintainer of the mencal
                              debian package:

                              I really love this script, because nobody but you ever bothered writing
                              it :-) I am very proud of you because you wrote this for your
                              girlfriend and I guess that was an act of love :-)

Valid HTML 4.01                                                                       Kyberdigi Labs

